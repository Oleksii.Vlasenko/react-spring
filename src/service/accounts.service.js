import axios from 'axios';

export const getAll = () =>
    axios.get('http://127.0.0.1:9000/accounts');

export const get = (id) =>
    axios.get(`http://127.0.0.1:9000/accounts/${id}`);

export const create = ({id, body}) =>
    axios.post(`http://127.0.0.1:9000/customers/${id}`,
        JSON.stringify(body),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        });

export const plus = (body) =>
    axios.post(`http://127.0.0.1:9000/accounts/topup`,
        JSON.stringify(body),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        });

export const minus = (body) =>
    axios.post(`http://127.0.0.1:9000/accounts/withdraw`,
        JSON.stringify(body),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        });

export const transfer = (body) =>
    axios.post(`http://127.0.0.1:9000/accounts/transfer`,
        JSON.stringify(body),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        });

export const remove = ({customerId, accountId}) =>
    axios.delete(`http://127.0.0.1:9000/customers/${customerId}/${accountId}`);


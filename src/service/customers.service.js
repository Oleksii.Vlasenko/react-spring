import axios from 'axios';

export const getAll = () =>
    axios.get('http://127.0.0.1:9000/customers');

export const get = (id) =>
    axios.get(`http://127.0.0.1:9000/customers/${id}`);

export const create = (body) =>
    axios.post(`http://127.0.0.1:9000/customers`,
        JSON.stringify(body),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        });

export const update = ({id, body}) =>
    axios.put(`http://127.0.0.1:9000/customers/${id}`,
        JSON.stringify(body),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        });

export const remove = (id) =>
    axios.delete(`http://127.0.0.1:9000/customers/${id}`);


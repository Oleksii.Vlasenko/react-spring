import React from "react";
import Dashboard from "../component/Dashboard";
import {BrowserRouter} from "react-router-dom";

function App() {
    return (
        <React.Fragment>
            <BrowserRouter>
                <Dashboard/>
            </BrowserRouter>
        </React.Fragment>
    );
}

export default App;

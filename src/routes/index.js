import React from "react";
import {Route, Switch} from "react-router";
import Customers from "../component/Dashboard/Customers";
import Accounts from "../component/Dashboard/Accounts";

const DashboardRoutes = () => {
    return (
        <Switch>
            <Route path="/customers" component={() => <Customers/>} exact/>
            <Route path="/accounts" component={() => <Accounts/>}/>
        </Switch>

    );


};

export default DashboardRoutes;

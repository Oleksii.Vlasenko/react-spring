import React from 'react';
import Button from "@material-ui/core/Button";
import SimpleModal from "../Modal";
import makeStyles from "@material-ui/core/styles/makeStyles";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import {Link} from "react-router-dom";
import {getAll, remove} from "../../../service/customers.service";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function Customers() {
    const classes = useStyles();
    const [customers, setCustomers] = React.useState([]);
    const [openModal, setOpenModal] = React.useState(false);
    const [isNewCustomer, setIsNewCustomer] = React.useState(true);
    const [currentId, setCurrentId] = React.useState(null);


    React.useEffect(() => {
        (async () => {
            try {
                await getAll().then(result => result.data).then(data => setCustomers(data));
            } catch (e) {
                console.log(e.message)
            }

        })();
    }, [])

    const removeCustomer = async (e) => {
        const target = e.target;
        let id = !target.getAttribute("customer") ?
            target.parentElement.getAttribute("customer") :
            target.getAttribute("customer");
        await remove(id);
    }

    const updateCustomer = (e) => {
        setIsNewCustomer(false);
        const target = e.target;
        let id = !target.getAttribute("customer") ?
            target.parentElement.getAttribute("customer") :
            target.getAttribute("customer");
        setCurrentId(id);
        setOpenModal(true);
    }

    const handleOpenModal = () => {
        setIsNewCustomer(true);
        setOpenModal(true);
    };

    const handleCloseModal = () => {
        setOpenModal(false);
    };

    return (
        <TableContainer component={Paper}>
            <Button variant="contained" color="primary" onClick={handleOpenModal}>
                <PersonAddIcon/>
            </Button>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="right">Email</TableCell>
                        <TableCell align="right">Age</TableCell>
                        <TableCell align="right">Remove</TableCell>
                        <TableCell align="right">Edit</TableCell>
                        <TableCell align="right">Accounts</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {customers && customers.map((item, index) => (
                        <TableRow key={item.name + index}>
                            <TableCell component="th" scope="row">
                                {item.name}
                            </TableCell>
                            <TableCell align="right">{item.email}</TableCell>
                            <TableCell align="right">{item.age}</TableCell>
                            <TableCell align="right">
                                <Button customer={item.id}
                                        variant="contained"
                                        color="primary"
                                        onClick={removeCustomer}>
                                    Remove
                                </Button>
                            </TableCell>
                            <TableCell align="right">
                                <Button customer={item.id}
                                        color="primary"
                                        variant="contained"
                                        onClick={updateCustomer}>
                                    Edit
                                </Button>
                            </TableCell>
                            <TableCell align="right">
                                <Link to={`/accounts?id=${item.id}`}>
                                    <AccountBalanceIcon/>
                                </Link>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <SimpleModal open={openModal}
                         action={() => handleCloseModal()}
                         isNewCustomer={isNewCustomer}
                         customer={currentId}/>
        </TableContainer>
    );
}

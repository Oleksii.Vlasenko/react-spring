import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {animated, useSpring} from "react-spring/web.cjs";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import {transfer} from "../../../../service/accounts.service";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

const Fade = React.forwardRef(function Fade(props, ref) {
    const {in: open, children, onEnter, onExited, ...other} = props;
    const style = useSpring({
        from: {opacity: 0},
        to: {opacity: open ? 1 : 0},
        onStart: () => {
            if (open && onEnter) {
                onEnter();
            }
        },
        onRest: () => {
            if (!open && onExited) {
                onExited();
            }
        },
    });

    return (
        <animated.div ref={ref} style={style} {...other}>
            {children}
        </animated.div>
    );
});

Fade.propTypes = {
    children: PropTypes.element,
    in: PropTypes.bool.isRequired,
    onEnter: PropTypes.func,
    onExited: PropTypes.func,
};

export default function TransferModal({open, action, account}) {
    const classes = useStyles();
    const [form, setForm] = useState({destination: null, amount: null});

    const send = async () => {
        await transfer({...form, id: account});
        action();
    }

    const changeHandler = (event) => {
        const target = event.target;
        setForm({...form,
            [target.name]: target.value})
    }

    return (
        <div>
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                className={classes.modal}
                open={open}
                onClose={action}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2 id="spring-modal-title">New customer</h2>
                        <div>
                            <FormControl className={classes.margin}>
                                <InputLabel htmlFor="input-with-icon-adornment">destination</InputLabel>
                                <Input onChange={changeHandler} name={"destination"} value={form.destination} id="destination"/>
                            </FormControl>
                        </div>
                        <div>
                            <FormControl className={classes.margin}>
                                <InputLabel htmlFor="input-with-icon-adornment">amount</InputLabel>
                                <Input onChange={changeHandler} name={"amount"} value={form.amount} id="amount"/>
                            </FormControl>
                        </div>
                        <Button onClick={send}>Send</Button>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

import React from 'react';
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";
import PlusModal from "./PlusModal";
import MinusModal from "./MinusModal";
import TransferModal from "./TransferModal";
import {get} from "../../../service/customers.service";
import {remove} from "../../../service/accounts.service";
import AccountModal from "../Customers/AccountModal";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function Accounts() {
    const classes = useStyles();
    const id = window.location.href.split('?id=')[1];

    const [customer, setCustomer] = React.useState({});
    const [openPlus, setOpenPlus] = React.useState(false);
    const [openMinus, setOpenMinus] = React.useState(false);
    const [openTransfer, setOpenTransfer] = React.useState(false);
    const [activeId, setActiveId] = React.useState(null);
    const [openAccountModal, setOpenAccountModal] = React.useState(false);

    React.useEffect(() => {
        (async () => {
            try {
                await get(id).then(result => result.data).then(data => setCustomer(data));
            } catch (e) {
                console.log(e.message)
            }
        })();
    }, [id])

    const setAccountId = (event) => {
        const target = event.target;
        setActiveId(!target.getAttribute("account") ?
            target.parentElement.getAttribute("account") :
            target.getAttribute("account"));
    }

    const handlePlus = (event) => {
        setAccountId(event);
        setOpenPlus(true);
    }

    const handleMinus = (event) => {
        setAccountId(event);
        setOpenMinus(true);
    }

    const handleTransfer = (event) => {
        setAccountId(event);
        setOpenTransfer(true);
    }

    const closeModal = () => {
        setOpenPlus(false);
        setOpenMinus(false);
        setOpenTransfer(false);
    };

    const handleOpenAccountModal = () => {
        setOpenAccountModal(true);
    }

    const handleCloseAccountModal = () => {
        setOpenAccountModal(false);
    }

    const removeAccount = async (e) => {
        const target = e.target;
        let accountId = !target.getAttribute("account") ?
            target.parentElement.getAttribute("account") :
            target.getAttribute("account");
        await remove({customerId: id, accountId: accountId})
    }

    return (
        <React.Fragment>
            <TableContainer component={Paper}>
                <h2>{customer.name}</h2>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">ID</TableCell>
                            <TableCell align="left">Account number</TableCell>
                            <TableCell align="right">Currency</TableCell>
                            <TableCell align="right">Balance</TableCell>
                            <TableCell align="right"></TableCell>
                            <TableCell align="right"></TableCell>
                            <TableCell align="center">Transfer</TableCell>
                            <TableCell align="center">Remove</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {customer.accounts && customer.accounts.map((item, index) => (
                            <TableRow key={item.id + index}>
                                <TableCell component="th" scope="row">{item.id}</TableCell>
                                <TableCell component="th" scope="row">{item.number}</TableCell>
                                <TableCell align="right">{item.currency}</TableCell>
                                <TableCell align="right">{item.balance}</TableCell>
                                <TableCell align="right">
                                    <Button variant="contained" color="primary" onClick={handlePlus} account={item.id}>
                                        Plus
                                    </Button>
                                </TableCell>
                                <TableCell align="right">
                                    <Button variant="contained" color="primary" onClick={handleMinus} account={item.id}>
                                        Minus
                                    </Button>
                                </TableCell>
                                <TableCell align="right">
                                    <Button variant="contained" color="primary" onClick={handleTransfer}
                                            account={item.id}>
                                        Transfer
                                    </Button>
                                </TableCell>
                                <TableCell align="right">
                                    <Button variant="contained" color="primary" onClick={removeAccount}
                                            account={item.id}>
                                        Remove
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Button variant="contained"
                    color="primary"
                    onClick={handleOpenAccountModal}>
                Create
            </Button>
            <PlusModal open={openPlus} action={closeModal} account={activeId}/>
            <MinusModal open={openMinus} action={closeModal} account={activeId}/>
            <TransferModal open={openTransfer} action={closeModal} account={activeId}/>
            <AccountModal open={openAccountModal}
                          action={() => handleCloseAccountModal()}
                          customer={id}
            />
        </React.Fragment>
    );
}

import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import {useSpring, animated} from 'react-spring/web.cjs';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import {create, update} from "../../../service/customers.service";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

const Fade = React.forwardRef(function Fade(props, ref) {
    const {in: open, children, onEnter, onExited, ...other} = props;
    const style = useSpring({
        from: {opacity: 0},
        to: {opacity: open ? 1 : 0},
        onStart: () => {
            if (open && onEnter) {
                onEnter();
            }
        },
        onRest: () => {
            if (!open && onExited) {
                onExited();
            }
        },
    });

    return (
        <animated.div ref={ref} style={style} {...other}>
            {children}
        </animated.div>
    );
});

Fade.propTypes = {
    children: PropTypes.element,
    in: PropTypes.bool.isRequired,
    onEnter: PropTypes.func,
    onExited: PropTypes.func,
};

export default function SimpleModal({open, action, isNewCustomer, customer}) {
    const classes = useStyles();
    const [form, setForm] = React.useState({name: null, email: null, age: null});

    const changeHandler = (event) => {
        const target = event.target;
        setForm({...form, [target.name]: target.value})
    }

    const send = async () => {
        if (isNewCustomer) {
            await create(form);
            action();
        } else {
            await update({id: customer, body: form});
        }
    }

    return (
        <div>
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                className={classes.modal}
                open={open}
                onClose={action}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2 id="spring-modal-title">New customer</h2>
                        <div>
                            <FormControl className={classes.margin} fullWidth={true}>
                                <InputLabel htmlFor="input-with-icon-adornment">Name</InputLabel>
                                <Input id="name"
                                       name={"name"}
                                       value={form.name}
                                       onChange={changeHandler}
                                />
                            </FormControl>
                        </div>
                        <div>
                            <FormControl className={classes.margin}>
                                <InputLabel htmlFor="input-with-icon-adornment">Email</InputLabel>
                                <Input id="email"
                                       name={"email"}
                                       value={form.email}
                                       onChange={changeHandler}
                                />
                            </FormControl>
                        </div>
                        <div>
                            <FormControl className={classes.margin}>
                                <InputLabel htmlFor="input-with-icon-adornment">Age</InputLabel>
                                <Input id="age"
                                       name={"age"}
                                       value={form.age}
                                       onChange={changeHandler}
                                />
                            </FormControl>
                        </div>
                        <Button onClick={send}>Create</Button>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
